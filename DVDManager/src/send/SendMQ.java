package send;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class SendMQ 
{
	 final static String QUEUE_NAME = "hello";	 
	
	public SendMQ(String message) throws IOException, TimeoutException{
		ConnectionFactory factory = new ConnectionFactory();
    	factory.setHost("localhost");
	    factory.setPort(5672);
    	Connection connection = factory.newConnection();
    	Channel channel = connection.createChannel();
    	channel.basicPublish("", QUEUE_NAME, null, message.getBytes());
       	channel.close();
    	connection.close();
	}
}






