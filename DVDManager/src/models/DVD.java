package models;

public class DVD {
	 private String title;
	    private int year;
	    private double price;
 
	     
	    
	    public DVD(String title, int year, double price) {
			super();
			this.title = title;
			this.year = year;
			this.price = price;
		}

		public void setTitle(String title)
	    {
	        this.title = title;
	    }

	    public String getTitle()
	    {
	        return title;
	    }

	    public void setYear(int year)
	    {
	        this.year = year;
	    }

	    public int getYear()
	    {
	        return year;
	    }

	    public void setPrice(double price)
	    {
	        this.price = price;
	    }

	    public double getPrice()
	    {
	        return price;
	    }

	    @Override
	    public String toString() {
	        return "DVD{" +
	                "title='" + title + '\'' +
	                ", year=" + year +
	                ", price=" + price +
	                '}';
	    }
}

