package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.DVD;
import send.SendMQ;

/**
 * Servlet implementation class DVDServlet
 */
@WebServlet("/DVDServlet")
public class DVDServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	SendMQ sender;
	
    /**
     * @throws TimeoutException 
     * @throws IOException 
     * @see HttpServlet#HttpServlet()
     */
    public DVDServlet() throws IOException, TimeoutException {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse
			response) throws ServletException, IOException {
			// TODO Auto-generated method stub
		
//			String firstName =request.getParameter("firstName");
//			String lastName =request.getParameter("lastName");
//			String cnp =request.getParameter("cnp");

			String dvdTitle=request.getParameter("dvd_title");
			String dvdYear=request.getParameter("dvd_year");
			String dvdPrice=request.getParameter("dvd_price");
			
			DVD aDVD = new DVD(dvdTitle, Integer.parseInt(dvdYear), Double.parseDouble(dvdPrice));
			//sender.sendMessage(aDVD.toString());
			try {
				sender = new SendMQ(aDVD.toString());
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			response.setContentType("text/html;charset=UTF-8");
			 PrintWriter out = response.getWriter();
			 try {
			 out.println(aDVD.toString());
			 } finally {
			 out.close();
			 }
	}

}








