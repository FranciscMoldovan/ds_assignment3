package services;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class TextFile {
	final String message;

    public TextFile(String message) throws FileNotFoundException{
        this.message = message;

        PrintWriter out = new PrintWriter("textfile.txt");
        out.println(message);
        out.close();
        System.out.println("File created.");
    }
}